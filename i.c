/*

  Adam Majewski
  fraktal.republika.pl

  c console progam using 
  * symmetry
  * openMP

  draw  julia sets



  gcc i.c -lm -Wall -fopenmp -march=native 
  time ./a.out
  time ./a.out > info.txt


  How to tune up parameter ? ( AR, t1p t1,  t1m , IterationMax )
  Make AR very big ( for whole image ). Then should see perfect target set !!!
    

*/



#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcat
#include <math.h> // M_PI; needs -lm also 
#include <complex.h>
#include <omp.h> // OpenMP; needs also -fopenmp


/* --------------------------------- global variables and consts ------------------------------------------------------------ */
#define iPeriodChild 3 // iPeriodChild of secondary component joined by root point
unsigned int denominator;
double InternalAngle;

// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
unsigned int ix, iy; // var
unsigned int ixMin = 0; // Indexes of array starts from 0 not 1
unsigned int ixMax ; //
unsigned int iWidth ; // horizontal dimension of array
unsigned int ixAxisOfSymmetry  ; // 
unsigned int iyMin = 0; // Indexes of array starts from 0 not 1
unsigned int iyMax ; //
unsigned int iyAxisOfSymmetry  ; // 
unsigned int iyAbove ; // var, measured from 1 to (iyAboveAxisLength -1)
unsigned int iyAboveMin = 1 ; //
unsigned int iyAboveMax ; //
unsigned int iyAboveAxisLength ; //
unsigned int iyBelowAxisLength ; //
unsigned int iHeight = 2000; //  odd number !!!!!! = (iyMax -iyMin + 1) = iyAboveAxisLength + iyBelowAxisLength +1
// The size of array has to be a positive constant integer 
unsigned int iSize ; // = iWidth*iHeight; 


// memmory 1D array 
unsigned char *data;
unsigned char *edge;

// unsigned int i; // var = index of 1D array
unsigned int iMin = 0; // Indexes of array starts from 0 not 1
unsigned int iMax ; // = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array


/* world ( double) coordinate = dynamic plane */
const double ZxMin=-1.5;
const double ZxMax=1.5;
const double ZyMin=-1.5;
const double ZyMax=1.5;
double PixelWidth; // =(ZxMax-ZxMin)/iXmax;
double PixelWidth2; // =  PixelWidth*PixelWidth;
double PixelHeight; // =(ZyMax-ZyMin)/iYmax;
double distanceMax;
double ratio ;
double TwoPi=2.0*M_PI;


 
long int iUknownPixels=0;
//static unsigned long int IterationMax  = 2000; //iHeight*100;

// complex numbers of parametr plane 
double Cx; // c =Cx +Cy * i
double Cy;
double complex c; // 

double complex Za; // alfa fixed point alfa=f(alfa)
double Zax, Zay;

unsigned long long int iterMax  = 1000000000; //iHeight*100;
double ER = 2.0; // Escape Radius for bailout test 
double ER2;


double AR ; // radius around attractor
double AR2; // =AR*AR;
double m = 10; 


// target set around alf with radius 0.001, between turns :        
double t1p ;//= 0.98995431602294066666 ; // 0.8232876493562740+(1/6) = .98995431602294066666
double t1 =  0.8232876493562740;
double t1m ; //= 0.65662098268960733334// 0.8232876493562740-(1/6) = 0.65662098268960733334

// c = -0.125000000000000  +0.649519052838329 i    okres = 10000
//z = -0.250000000000000  +0.433012701892219 i



unsigned char  iColorsOfInterior[iPeriodChild]={140, 180,220}; // number of colors >= iPeriodChild
static unsigned char iColorOfExterior = 245;
//static unsigned char iColorOfUnknown = 100;

/* ------------------------------------------ functions -------------------------------------------------------------*/



/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(double InternalAngleInTurns, double InternalRadius, unsigned int Period)
{
  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double t = InternalAngleInTurns *2*M_PI; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  //double Cx, Cy; /* C = Cx+Cy*i */
  switch ( Period ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each iPeriodChild  there are 2^(iPeriodChild-1) roots. 
    default: // higher periods : to do
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}


/*

  http://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
  z^2 + c = z
  z^2 - z + c = 0
  ax^2 +bx + c =0 // ge3neral for  of quadratic equation
  so :
  a=1
  b =-1
  c = c
  so :

  The discriminant is the  d=b^2- 4ac 

  d=1-4c = dx+dy*i
  r(d)=sqrt(dx^2 + dy^2)
  sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx +- sy*i

  x1=(1+sqrt(d))/2 = beta = (1+sx+sy*i)/2

  x2=(1-sqrt(d))/2 = alfa = (1-sx -sy*i)/2

  alfa : attracting when c is in main cardioid of Mandelbrot set, then it is in interior of Filled-in Julia set, 
  it means belongs to Fatou set ( strictly to basin of attraction of finite fixed point )

*/
// uses global variables : 
//  ax, ay (output = alfa(c)) 
double complex GiveAlfaFixedPoint(double complex c)
{
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  double ax, ay;
 
  // d=1-4c = dx+dy*i
  dx = 1 - 4*creal(c);
  dy = -4 * cimag(c);
  // r(d)=sqrt(dx^2 + dy^2)
  r = sqrt(dx*dx + dy*dy);
  //sqrt(d) = s =sx +sy*i
  sx = sqrt((r+dx)/2);
  sy = sqrt((r-dx)/2);
  // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
  ax = 0.5 - sx/2.0;
  ay =  sy/2.0;
 

  return ax+ay*I;
}




int setup()
{

  
  

  denominator = iPeriodChild;
  InternalAngle = 1.0/((double) denominator);

  c = GiveC(InternalAngle, 1.0, 1) ;
  Cx=creal(c);
  Cy=cimag(c);
  Za = GiveAlfaFixedPoint(c);
  Zax = creal(Za);
  Zay = cimag(Za);



  //

  t1p = t1 + 1.0/(2.0*iPeriodChild);
  t1m = t1 - 1.0/(2.0*iPeriodChild);

 

  /* 2D array ranges */
  if (!(iHeight % 2)) iHeight+=1; // it sholud be even number (variable % 2) or (variable & 1)
  iWidth = iHeight;
  iSize = iWidth*iHeight; // size = number of points in array 
  // iy
  iyMax = iHeight - 1 ; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  iyAboveAxisLength = (iHeight -1)/2;
  iyAboveMax = iyAboveAxisLength ; 
  iyBelowAxisLength = iyAboveAxisLength; // the same 
  iyAxisOfSymmetry = iyMin + iyBelowAxisLength ; 
  // ix
  
  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize-1; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].


  /* Pixel sizes */
  PixelWidth = (ZxMax-ZxMin)/ixMax; //  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (ZyMax-ZyMin)/iyMax;
  ratio = ((ZxMax-ZxMin)/(ZyMax-ZyMin))/((float)iWidth/(float)iHeight); // it should be 1.000 ...
  distanceMax = PixelWidth; 
  

  // for numerical optimisation 
  ER2 = ER * ER;
  AR = PixelWidth*m; // !!!! important value  ( and precision and time of creation of the pgm image ) 
  AR2= AR*AR;
  PixelWidth2 =  PixelWidth*PixelWidth;
 
  /* create dynamic 1D arrays for colors ( shades of gray ) */
  
  data = malloc( iSize * sizeof(unsigned char) );
  edge = malloc( iSize * sizeof(unsigned char) );
  if (edge == NULL || edge == NULL)
    {
      fprintf(stderr," Could not allocate memory\n");
      return 1;
    }
  else fprintf(stderr," memory is OK \n");

  
 
  return 0;

}



// from screen to world coordinate ; linear mapping
// uses global cons
double GiveZx(unsigned int ix)
{ return (ZxMin + ix*PixelWidth );}

// uses globaal cons
double GiveZy(unsigned int iy)
{ return (ZyMax - iy*PixelHeight);} // reverse y axis






double GiveTurn(double x, double y)
{
  double argument;

  argument = atan2 (y, x); // carg(z)   argument in radians from -pi to pi
  if (argument<0) argument=argument + TwoPi; //   argument in radians from 0 to 2*pi
  return argument/TwoPi ; // argument in turns from 0.0 to 1.0
}




unsigned char GiveColor(unsigned int ix, unsigned int iy)
{ 
  // check behavour of z under fc(z)=z^2+c
  // using 2 target set:
  // 1. exterior or circle (center at origin and radius ER ) 
  // as a target set containing infinity = for escaping points ( bailout test)
  // for points of exterior of julia set
  // 2. interior of circle with center = alfa and radius dMaxDistance2Alfa
  // as a target set for points of interior of Julia set 
  //  Z= Zx+ZY*i;

  double Zx2, Zy2;
  int i=0;
  //int j; // iteration = fc(z)
  double d2 ; /* d2= (distance from z to Alpha)^2   */
  double Zxt,Zyt ; // 
  double Zx, Zy;
  double turn;
  unsigned char iColor;
  //int i; 
  
  
  // from screen to world coordinate 
  Zx = GiveZx(ix);
  Zy = GiveZy(iy);
  /* distance from z to Alpha  */
  Zxt=Zx-Zax;
  Zyt=Zy-Zay;
  d2=Zxt*Zxt +Zyt*Zyt;
  if (d2<AR2) 
    {  
      turn = GiveTurn(Zxt, Zyt);
      //if (0.98995431602294066666<turn || turn<=0.32328764935627) 
      iColor = iColorsOfInterior[0]; 
      if (0.32328764935627<turn && turn<=0.65662098268960733334) iColor = iColorsOfInterior[1];
      if (0.65662098268960733334<turn && turn<=0.98995431602294066666) iColor = iColorsOfInterior[2];
       
      return iColor;
    }
    

  // if not inside target set around attractor ( alfa fixed point )
  while (d2>AR2 )
    { // then iterate 
     

    
      for(i=0;i<iPeriodChild ;++i) // iMax = period !!!!
	{  
	  Zx2 = Zx*Zx; 
	  Zy2 = Zy*Zy;
       
	  // bailout test 
	  if (Zx2 + Zy2 > ER2) return iColorOfExterior; // if escaping stop iteration
       
	  // if not escaping or not attracting then iterate = check behaviour
	  // new z : Z(n+1) = Zn * Zn  + C
	  Zy = 2*Zx*Zy + Cy; 
	  Zx = Zx2 - Zy2 + Cx; 
	  //
	  //i+=1;
	 
     
   
	}
      /* distance from z to Alpha  */
      Zxt=Zx-Zax;
      Zyt=Zy-Zay;
      d2=Zxt*Zxt +Zyt*Zyt;
      // check if fall into internal target set 
      //if (i % iPeriodChild) 

      //if (i > IterationMax) break; // unknown
      
      
    }

  //Color = iColorsOfInterior[i % iPeriodChild];
  if (d2<AR2  ) 
    {  
      turn = GiveTurn(Zxt, Zyt);
      //if (0.98995431602294066666<turn || turn<=0.32328764935627) 
      iColor = iColorsOfInterior[0];
      if (0.32328764935627<turn && turn<=0.65662098268960733334) iColor = iColorsOfInterior[1];
      if (0.65662098268960733334<turn && turn<=0.98995431602294066666) iColor = iColorsOfInterior[2];

      // return iColor;
    }
    
       
  return iColor;
}


 




/* -----------  array functions -------------- */


/* gives position of 2D point (iX,iY) in 1D array  ; uses also global variable iWidth */
unsigned int Give_i(unsigned int ix, unsigned int iy)
{ return ix + iy*iWidth; }
//  ix = i % iWidth;
//  iy = (i- ix) / iWidth;
//  i  = Give_i(ix, iy);




// plots raster point (ix,iy) 
int PlotPoint(unsigned int ix, unsigned int iy, unsigned char iColor)
{
  unsigned i; /* index of 1D array */
  i = Give_i(ix,iy); /* compute index of 1D array from indices of 2D array */
  data[i] = iColor;

  return 0;
}


// fill array 
// uses global var :  ...
// scanning complex plane 
int FillArray(unsigned char data[] )
{
  unsigned int ix, iy; // pixel coordinate 


  // for all pixels of image 
  for(iy = iyMin; iy<=iyMax; ++iy) 
    { printf(" %d z %d\r", iy, iyMax); //info 
      for(ix= ixMin; ix<=ixMax; ++ix) PlotPoint(ix, iy, GiveColor(ix, iy) ); //  
    } 
   
  return 0;
}


// fill array using symmetry of image 
// uses global var :  ...
int FillArraySymmetric(unsigned char data[] )
{
   
  unsigned char Color; // gray from 0 to 255 

  printf("axis of symmetry \n"); 
  iy = iyAxisOfSymmetry; 
#pragma omp parallel for schedule(dynamic) private(ix,Color) shared(ixMin,ixMax, iyAxisOfSymmetry)
  for(ix=ixMin;ix<=ixMax;++ix) {//printf(" %d from %d\n", ix, ixMax); //info  
    PlotPoint(ix, iy, GiveColor(ix, iy));
  }


  /*
    The use of ‘shared(variable, variable2) specifies that these variables should be shared among all the threads.
    The use of ‘private(variable, variable2)’ specifies that these variables should have a seperate instance in each thread.
  */

#pragma omp parallel for schedule(dynamic) private(iyAbove,ix,iy,Color) shared(iyAboveMin, iyAboveMax,ixMin,ixMax, iyAxisOfSymmetry)

  // above and below axis 
  for(iyAbove = iyAboveMin; iyAbove<=iyAboveMax; ++iyAbove) 
    {printf(" %d from %d\r", iyAbove, iyAboveMax); //info 
      for(ix=ixMin; ix<=ixMax; ++ix) 

	{ // above axis compute color and save it to the array
	  iy = iyAxisOfSymmetry + iyAbove;
	  Color = GiveColor(ix, iy);
	  PlotPoint(ix, iy, Color ); 
	  // below the axis only copy Color the same as above without computing it 
	  PlotPoint(ixMax-ix, iyAxisOfSymmetry - iyAbove , Color ); 
	} 
    }  
  return 0;
}

int AddBoundaries(unsigned char data[])
{

  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
  /* sobel filter */
  unsigned char G, Gh, Gv; 
 
  


  printf(" find boundaries in data array using  Sobel filter\n");   
#pragma omp parallel for schedule(dynamic) private(i,iY,iX,Gv,Gh,G) shared(iyMax,ixMax, ER2)
  for(iY=1;iY<iyMax-1;++iY){ 
    for(iX=1;iX<ixMax-1;++iX){ 
      Gv= data[Give_i(iX-1,iY+1)] + 2*data[Give_i(iX,iY+1)] + data[Give_i(iX-1,iY+1)] - data[Give_i(iX-1,iY-1)] - 2*data[Give_i(iX-1,iY)] - data[Give_i(iX+1,iY-1)];
      Gh= data[Give_i(iX+1,iY+1)] + 2*data[Give_i(iX+1,iY)] + data[Give_i(iX-1,iY-1)] - data[Give_i(iX+1,iY-1)] - 2*data[Give_i(iX-1,iY)] - data[Give_i(iX-1,iY-1)];
      G = sqrt(Gh*Gh + Gv*Gv);
      i= Give_i(iX,iY); /* compute index of 1D array from indices of 2D array */
      if (G==0) {edge[i]=255;} /* background */
      else {edge[i]=0;}  /* boundary */
    }
  }

  // copy boundaries from edge array to data array 
  for(iY=1;iY<iyMax-1;++iY){ 
    for(iX=1;iX<ixMax-1;++iX){i= Give_i(iX,iY); if (edge[i]==0) data[i]=0;}}



  return 0;
}


// Check Orientation of image : first quadrant in upper right position
// uses global var :  ...
int CheckOrientation(unsigned char data[] )
{
  unsigned int ix, iy; // pixel coordinate 
  double Zx, Zy; //  Z= Zx+ZY*i;
  unsigned i; /* index of 1D array */
  for(iy=iyMin;iy<=iyMax;++iy) 
    {
      Zy = GiveZy(iy);
      for(ix=ixMin;ix<=ixMax;++ix) 
	{

	  // from screen to world coordinate 
	  Zx = GiveZx(ix);
	  i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
	  if (Zx>0 && Zy>0) data[i]=255-data[i];   // check the orientation of Z-plane by marking first quadrant */

	}
    }
   
  return 0;
}


double GiveFatou(double x, double y)
{
  double t = (y*y+x*x);
  t = t*t*t;
  return -(3*x*x*y-y*y*y)/t;

}


int MakeInternalTiling(unsigned char data[] )
{
  unsigned int ix, iy; // pixel coordinate 
  double Zx, Zy; //  Z= Zx+ZY*i;
  unsigned i; // index of 1D array ; do noit use i for iteration !!!!!
  int iter;
  int iterp; // iter modulu period
  

  double Zx2, Zy2;
  double d2 ; /* d2= (distance from z to Alpha)^2   */
  double Zxt,Zyt ; // 
  //double Zx, Zy;

  for(iy=iyMin;iy<=iyMax;++iy) 
    
     {
      for(ix=ixMin;ix<=ixMax;++ix) 
	{

	  // from screen to world coordinate 
	  
	  i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
	  if (data[i] == iColorsOfInterior[0] || data[i] == iColorsOfInterior[1] || data[i] == iColorsOfInterior[2] ) //{data[i]=50; printf("yes \n");  }
          //else printf("no\n");
	    {
            
             iterp = 0;
             Zy = GiveZy(iy);
             Zx = GiveZx(ix);
             // distance from z to Alpha  
	     Zxt=Zx-Zax;
	     Zyt=Zy-Zay;
	     d2=Zxt*Zxt +Zyt*Zyt;

	     while (d2>AR2 )
		{ // then iterate 
      

    
		  for(iter=0;iter<iPeriodChild ;++iter) // iMax = period !!!!
		    {  
		      Zx2 = Zx*Zx; 
		      Zy2 = Zy*Zy;
       
		      // new z : Z(n+1) = Zn * Zn  + C
		      Zy = 2*Zx*Zy + Cy; 
		      Zx = Zx2 - Zy2 + Cx; 
		      //
		      
	 
     
   
		    }
                  // distance from z to Alpha  
		  Zxt=Zx-Zax;
		  Zyt=Zy-Zay;
		  d2=Zxt*Zxt +Zyt*Zyt;
		  iterp+=1; 
		  
      
		     

             // f = GiveFatou(Zx, Zy); 
	     // if (0.0< f) data[i] += 16;
            
              //printf( "fatou = %f \n",f ); 
           }  // 
         // if (0.833<GiveTurn(Zxt, Zyt) ) data[i] += 16;
           //                          else data[i] -= 16;
          
        // printf(" iter mod 2 = %d ; data old = % d ; ", iter % 2, data[i] );
         data[i]=  iterp % 255;
          //printf(" new =  %d \n", data[i] );
	}

       
    }
   }
  printf("done\n");
  return 0;
}




// save data array to pgm file 
int SaveArray2PGMFile( unsigned char data[], double k)
{
  
  FILE * fp;
  const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name [50]; /* name of file */
  sprintf(name,"%.1f", k); /*  */
  char *filename =strcat(name,".pgm");
  char *comment="# ";/* comment should start with # */

  /* save image to the pgm file  */      
  fp= fopen(filename,"wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"P5\n %s\n %u %u\n %u\n",comment,iWidth,iHeight,MaxColorComponentValue);  /*write header to the file*/
  fwrite(data,iSize,1,fp);  /*write image data bytes to the file in one step */
  printf("File %s saved. \n", filename);
  fclose(fp);

  return 0;
}


int info()
{
  // diplay info messages
  printf("Cx  = %f \n", Cx); 
  printf("Cy  = %f \n", Cy);
  // 
 
  printf("Z_alfa)  = %f ; %f \n", Zax, Zay);
  printf("iHeght  = %d \n", iHeight);
  printf("Unknown pixels = %ld ; it should be 0 ...\n", iUknownPixels);
  printf("ratio ( distortion) of image  = %f ; it should be 1.000 ...\n", ratio);
  return 0;
}


/* -----------------------------------------  main   -------------------------------------------------------------*/
int main()
{
  // here are procedures for creating image file

  setup();
  
  // compute colors of pixels = image
  //FillArray( data ); // no symmetry
  FillArraySymmetric(data); 
  AddBoundaries(data);
  //  CheckOrientation( data );
  SaveArray2PGMFile(data , m); // save array (image) to pgm file 


  MakeInternalTiling(data);
  SaveArray2PGMFile(data , m+0.2); // save array (image) to pgm file 

  CheckOrientation(data);
  SaveArray2PGMFile(data , m+0.3); // save array (image) to pgm file

  free(data);
  free(edge);
  info();
  return 0;
}
